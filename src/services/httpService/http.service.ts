import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, ResponseContentType } from "@angular/http";

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Order } from '../../app/order';
import { Product } from '../../app/product';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {
    private baseUrl = ' https://sisweb-product-orders.herokuapp.com';
    public instance: any;

    constructor(private http: Http) {
    }
    
    public get(url: string) {
        let headers = this.setHeaders();
        return this.http.get(this.baseUrl + url, { headers })
            .map((response) => {
                return response.json();
            })
            .catch(handleError);
    }

    public put(url: string, params: any) {
        let headers = this.setHeaders();
        return this.http.put(this.baseUrl + url, params, { headers })
            .map((response) => {
                return response.json();
            })
            .catch(handleError);
    }

    public post(url: string, params: any) {
        let headers = this.setHeaders();
        return this.http.post(this.baseUrl + url, params, { headers })
            .map((response) => {
                return response.json();
            })
            .catch(handleError);
    }

    public delete(url: string) {
        let headers = this.setHeaders();
        return this.http.delete(this.baseUrl + url, { headers })
            .map((response) => {
                return response.json();
            })
            .catch(handleError);
    }

    private setHeaders() {
        let headers = new Headers();
        headers.append('user','5214110');
        headers.append('Content-Type', 'application/json');
        return headers;
    }
}

function handleError(error: any) {
    console.error('An error occurred', error);
    return Observable.throw(error.message || error);
}