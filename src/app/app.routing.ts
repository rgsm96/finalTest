import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import{ NewOrderCmpt }   from '../views/newOrder/newOrder.cmpt';
import { ShowProductCmpt }   from '../views/showProduct/showProduct.cmpt';
import { TabsCmpt }   from '../views/tabs/tabs.cmpt';

import { DraftedCmpt }   from '../views/drafted/drafted.cmpt';
import { RequestedCmpt }   from '../views/requested/resquested.cmpt';
import { CompletedCmpt }   from '../views/completed/completed.cmpt';


const routes: Routes = [
  { path: '', redirectTo: '/newOrder', pathMatch: 'full' },
  { path: 'newOrder', component: NewOrderCmpt },
  { path: 'tabs', component: TabsCmpt },
  { path: 'showProduct/:id', component: ShowProductCmpt },
  { path: 'requested', component: RequestedCmpt },
  { path: 'drafted', component: DraftedCmpt },
  { path: 'completed', component: CompletedCmpt },
  
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}