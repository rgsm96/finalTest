import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule, JsonpModule } from '@angular/http';
import { HttpClientModule }    from '@angular/common/http';


import { AppComponent }         from './app.component';
import { NewOrderCmpt }   from '../views/newOrder/newOrder.cmpt';
import { ShowProductCmpt }   from '../views/showProduct/showProduct.cmpt';
import { TabsCmpt }   from '../views/tabs/tabs.cmpt';

import { DraftedCmpt }   from '../views/drafted/drafted.cmpt';
import { CompletedCmpt }   from '../views/completed/completed.cmpt';
import { RequestedCmpt }   from '../views/requested/resquested.cmpt';

import { AppRoutingModule }     from './app.routing';

import { HttpService } from '../services/httpService/http.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    JsonpModule,
  ],
  declarations: [
    AppComponent,
    NewOrderCmpt,
    ShowProductCmpt,
    TabsCmpt,
    DraftedCmpt, 
    CompletedCmpt,
    RequestedCmpt
  ],
  providers:[
    HttpService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }


