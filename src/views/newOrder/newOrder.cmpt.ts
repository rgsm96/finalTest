import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/httpService/http.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Router } from '@angular/router';
/* 
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
 */
@Component({
  selector: 'new-order',
  templateUrl: './newOrder.cmpt.html',
  /* styleUrls: ['./heroes.component.css'] */
})
export class NewOrderCmpt implements OnInit {
  private fieldArray: any = [];
  private newAttribute: any = {};
  private products:any=[];
  private newProduct:any;
  private client:any={};
  private productsAvailable:any=[];
  private flag: boolean = false;
  private edited: boolean = false;
  constructor(private httpService: HttpService , private router: Router) { }
  
  ngOnInit() {
    this.httpService.get("/products").subscribe(response =>{
      this.productsAvailable = response;
    });
  }

  addFieldValue() {
    this.flag=true;
    this.fieldArray.push(this.newProduct)
    this.newProduct = {};
  }

  saveOrder(){
    let body = {
      customer : this.client.name,
      products : this.fieldArray,
      status : "draft"
    }    
    this.httpService.post("/order",JSON.stringify(body)).subscribe(response=>{
      console.log(response);
    })
    this.router.navigate(['/drafted']);
    
  }
  sendOrder(){
    let body = {
      customer : this.client.name,
      products : this.fieldArray,
      status : "requested"
    }    
    this.httpService.post("/order",JSON.stringify(body)).subscribe(response=>{
      console.log(response);
    })
    this.router.navigate(['/requested']);
  }
}