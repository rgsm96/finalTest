import { Component, OnInit  } from '@angular/core';
import { HttpService } from '../../services/httpService/http.service';
import { ActivatedRoute,Params } from '@angular/router';
import { Location } from '@angular/common';
/* 
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
 */
@Component({
  selector: 'show-product',
  templateUrl: './showProduct.cmpt.html',
  /* styleUrls: ['./heroes.component.css'] */
})
export class ShowProductCmpt implements OnInit {
  private products : any =[];

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private httpService: HttpService ,
    private activedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    
    this.activedRoute.params.subscribe((params:Params)=>{
        let id=params['id'];
        this.httpService.get("/order/"+id).subscribe(response=>{  
            this.products=response.products;
        });
    })

  }

}