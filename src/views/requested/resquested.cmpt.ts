import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/httpService/http.service';
@Component({
  selector: 'requested',
  templateUrl: './requested.cmpt.html',
})
export class RequestedCmpt implements OnInit {
  private orderArray:any=[];
  constructor(private httpService: HttpService ) { }

  ngOnInit() {

      this.httpService.get("/orders?status=requested").subscribe(response=>{
        this.orderArray=response;
      });
  }
}