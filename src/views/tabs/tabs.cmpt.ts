import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'tabs',
  templateUrl: './tabs.cmpt.html',
  /* styleUrls: ['./heroes.component.css'] */
})
export class TabsCmpt implements OnInit {
//   heroes: Hero[];

  constructor(private router: Router) { }

  ngOnInit() {
    // this.getHeroes();
  }
  redirectToNewOrder(){
    this.router.navigate(['/newOrder']);
  }
}