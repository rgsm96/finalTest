import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/httpService/http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'drafted',
  templateUrl: './drafted.cmpt.html',
})

export class DraftedCmpt implements OnInit {
  private orderArray:any=[];
  constructor(private httpService: HttpService,private router: Router) { }

  ngOnInit() {
      this.httpService.get("/orders?status=draft").subscribe(response=>{
        this.orderArray=response;
      });
  }
  deleteOrder(id : any){
    this.httpService.delete("/order/{{id}}").subscribe(response=>{
      console.log(response);
    });
  }
  confirmOrder(){
    this.orderArray.forEach((e : any) => {
      let body = {
        customer : e.customer,
        products : e.products,
        status : "requested"
      }
      this.httpService.put("/order/{{e.order._i}}",body).subscribe(response=>{
        console.log(response);
      });
    });
    this.router.navigate(['/drafted']);
    
  }
}